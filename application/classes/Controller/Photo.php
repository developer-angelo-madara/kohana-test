<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Photo extends Controller {
	
	public function action_index(){
		$photos = (new Model_Photo())->get();
		// print_r($photos);
		$this->response->body(View::factory('photo')->bind('photos',$photos));
	}

	public function action_post(){

		$post = $this->request->post();

		if(!$post['title']){
			$this->response->body(json_encode([
				'message' => "Photo doesn't have name",
				'status' => false
			]));
			return false;
		}

		if(!$_FILES){
			$this->response->body(json_encode([
				'message' => "File is empty",
				'status' => false
			]));
			return false;
		}

		$file = $_FILES['filename'];		
		$this->upload_image($post,$file,'add');
	}

	protected function upload_image($post,$image,$type){
		$photo_model = new Model_Photo();
		
		if(
			Upload::valid($image) &&
			Upload::not_empty($image) &&
			Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))
		){
			
			$directory = PUBLIC_DIR.'images/'; 
			if ($file = Upload::save($image, NULL, $directory)){
				$big = 'big_'.strtolower(Text::random('alnum', 20)).'.jpg';
				$thumb = 'thumb_'.strtolower(Text::random('alnum', 20)).'.jpg';
				
				/** get the actual width and height of the picture */
				$data = getimagesize($file);
				$width = $data[0];
				$height = $data[1];

				if($type == 'add'){
					$photo_model->insert(
						['title','thumbnail','filename','created_at'],
						[$post['title'],'public/images/'.$thumb,'public/images/'.$big,date('Y-m-d')]
					);
				}
				elseif($type == 'edit'){
					$photo_model->edit(
						[
							'title' => $post['title'],
							'thumbnail' => 'public/images/'.$thumb,
							'filename' => 'public/images/'.$big,
							'created_at' => date('Y-m-d')
						],'id',$post['id']
					);
				}
				
				
				$photos = $photo_model->get();

				/** thumbnail */
				Image::factory($file)
					->resize(400, 400, Image::NONE)
					->save($directory.$thumb);
				/** big picture */
				Image::factory($file)
					->resize($width, $height, Image::AUTO)
					->save($directory.$big);
				/** Delete the temporary file */
				unlink($file);

				$this->response->body(
					json_encode([
						'message' => 'Photo successufully ' . ($type == 'add' ? 'uploaded' : 'updated'),
						'status' => true,
						'data' => $photos->as_array()
					])
				);
				

				return false;
			}

		}else{
			$this->response->body(json_encode([
				'message' => "Invalid file type",
				'status' => false
			]));
		}
	}

	public function action_remove(){
		$post = $this->request->post();
		(new Model_Photo())->remove($post['id']);
		
		$this->response->body(
			json_encode([
				'message' => 'Photo successufully removed',
				'status' => true,
			])
		);
	}

	public function action_edit(){
		$post = $this->request->post();

		if(!$post['title']){
			$this->response->body(json_encode([
				'message' => "Photo doesn't have name",
				'status' => false
			]));
			return false;
		}

		if(!$_FILES){
			$this->response->body(json_encode([
				'message' => "File is empty",
				'status' => false
			]));
			return false;
		}

		$file = $_FILES['filename'];
		$this->upload_image($post,$file,'edit');
	}
} 
