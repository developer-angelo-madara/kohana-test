<?php

class Model_Photo extends Model_Database {
    
    public function get(){
        return DB::select()->from('photos')->order_by('id', 'DESC')->execute();
    }

    public function insert($columns,$values){
        DB::insert('photos',$columns)->values($values)->execute();
    }

    public function remove($id){
        DB::delete('photos')->where('id','=',$id)->execute();
    }

    public function edit($set,$ref,$val){
        DB::update('photos')->set($set)->where($ref, '=', $val)->execute();
    }
}