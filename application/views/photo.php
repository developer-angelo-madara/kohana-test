<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Photo Upload App</title>
    <link rel="stylesheet" href="public/css/style.css">
</head>
<body>
    
    <div class="title clearfix">
        <h1 class="center-text">PHOTO APP</h1>
        <button class="float-right button" id="add-record"><b>+</b> Add Record</button>
    </div>

    <div class="clearfix">

        <!-- application list of photos -->
        <div class="float-left width-100">
            <h3 class="padding-10">List of photos</h3>
            
            <div class="clearfix padding-10" id="images">
            <?php foreach ($photos as $photo) : ?>
                <div class="width-20 float-left thumbnail-holder" data-id="<?= $photo['id'] ?>" data-name="<?=$photo['title']?>">
                    <span class='edit'>&#9998;</span>
                    <span class='remove'>&#10005;</span>
                    <img src="<?= '/kohana/' . $photo['thumbnail'] ?>" alt="<?= $photo['filename'] ?>">
                    <?= mb_strimwidth($photo['title'], 0, 12, '...') ?>
                </div>
            <?php endforeach; ?>
            </div>            
        </div>

        

    </div>

    <!-- application form/modal -->
    <div class="modal hide">
        <div class="m-title">Upload new photo</div>
        <div class="m-body">
            <form action="">
                <label class="form-group">
                    <div>Title</div>
                    <input type="text" name="title" id="title">
                </label>
                <br>
                <label class="form-group">
                    <div>Photo</div>
                    <input type="file" name="filename" id="filename">
                </label>
            </form>
        </div>
        <div class="m-foot clearfix">
            <button class="button float-right" id="upload-photo">Add Photo</button>
            <button class="button float-right" id="close-modal">Close</button>
        </div>
    </div>
    <!-- application alert -->
    <div class="alert hide">
        <div class="alert-body"></div>
        <div class="alert-foot clearfix">
            <button class="button float-right" id="close-alert">Close</button>
        </div>
    </div>
    <div class="modal-background hide"></div>

    <script src="public/js/jquery.js" type="text/javascript"></script>
    <script src="public/js/app.js" type="text/javascript"></script>
</body>
</html>