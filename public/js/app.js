(function () {
    $(document)
        .on('click', '#add-record,#close-modal', function () {
            toggleModal('Upload new photo');
        })
        .on('click', '#close-alert', function () {
            toggleAlert();
        })
        .on('click', '#upload-photo', function () {
            var data = new FormData();
            data.append('title', $("input[name='title']").val());
            data.append('filename', $("input[name='filename']")[0].files[0]);
            var uri = "";
            if($(this).text() == 'Add Photo'){
                uri = "photo/post";
            }else{
                uri = 'photo/edit';
                data.append('id', $("input[name='id']").val());
            }
            $.ajax({
                method: "POST",
                url: uri,
                data: data,
                processData: false,
                contentType: false,
            }).done(function (msg) {
                var msg = JSON.parse(msg);
                if (msg.status == false) {
                    toggleAlert(msg.message);
                } else {
                    let div_images = $("#images");
                    let images = msg.data;
                    var element = '';
                    var title = '';
                    images.forEach(image => {

                        if (image.title.length > 12) {
                            title = image.title.substring(0, 12) + '...';
                        } else {
                            title = image.title
                        }

                        element += '<div class="width-20 float-left thumbnail-holder" data-id="'+image.id+'" data-name="'+image.title+'">' +
                            '<span class=\'edit\'>&#9998;</span>'+
                            '<span class=\'remove\'>&#10005;</span>'+
                            '<img src="' + image.thumbnail + '" alt="' + image.title + '">' +
                            title +
                            '</div>';
                    });
                    div_images.html(element);
                    toggleModal();
                    toggleAlert(msg.message);
                }
                $("input[name='id']").remove();
            });
        })
        .on('click', '.remove', function () {
            $('#yes-delete').remove();
            let el = $(this).closest('.thumbnail-holder');
            $(".alert-foot").prepend('<button class="button float-right" id="yes-delete" data-id="'+el.data('id')+'">Yes</button>');
            toggleAlert('Remove '+el.data('name')+'?');
        })
        .on('click','#yes-delete',()=>{
            let id = $('#yes-delete').data('id');
            $(".alert").toggleClass('show', 'hide');
            $(".modal-background").toggleClass('show', 'hide');
            $.ajax({
                method: "POST",
                url: "photo/remove",
                data: { 'id': id },
            }).done(function (msg) {
                $('#yes-delete').remove();
                var msg = JSON.parse(msg);
                if (msg.status == false) {
                    toggleAlert(msg.message);
                } else {
                    toggleAlert(msg.message);
                    setTimeout(()=>{$('.thumbnail-holder[data-id="'+id+'"]').remove();},500);
                }
            });
        })
        .on('click','.edit',function(){
            $("input[name='id']").remove();
            let el = $(this).closest('.thumbnail-holder');
            $('form').append('<input type="hidden" value="'+el.data('id')+'" name="id">');
            toggleModal('Update '+el.data('name'));
            $('input#title').val(el.data('name'));
            $('button#upload-photo').html('Update');
        })

    function toggleModal(title) {
        $("form").trigger("reset");
        $('button#upload-photo').html('Add Photo');
        $(".modal").toggleClass('show', 'hide');
        $(".modal > .m-title").html(title);
        $(".modal-background").toggleClass('show', 'hide');
    }

    function toggleAlert(msg) {
        $('.alert-body').html(msg);
        $(".alert").toggleClass('show', 'hide');
        $(".modal-background").toggleClass('show', 'hide');
    }
}());